import glob
import os
from re import search
import shutil
import music21 as m21


class Midi_track:
    def __init__(self, id, name, genre, bpm, midi):
        self.id = id
        self.name = name
        self.genre = genre
        self.bpm = bpm
        self.midi = midi

UNPROCESSED_DATASET_PATH = "groove_dataset_unprocessed"
PROCESSED_DATASET_PATH = "groove_dataset_processed"

parent_dir = "D:\\Mcast\\MCAST\\Repos\\as_2022_thesis\\"
genres_dictionary = {"fuzed_jazz" : 0, "fuzed_rock" : 0, "fuzed_hiphop" : 0, "fuzed_funk" : 0, "fuzed_afrocuban" : 0}
genres = ["fuzed_jazz", "fuzed_rock", "fuzed_hiphop", "fuzed_funk", "fuzed_afrocuban"]



def get_fuzed_genre(genre):
    if search("hiphop", genre):
        return "fuzed_hiphop"
    elif search("jazz", genre):
        return "fuzed_jazz"
    elif search("rock", genre):
        return "fuzed_rock"
    elif search("funk", genre):
        return "fuzed_funk"
    elif search("afrocuban", genre):
        return "fuzed_afrocuban"
    else:
        return "invalid"


def append_genre_count(genre):
    global genres_dictionary
    
    _genre = get_fuzed_genre(genre)

    if _genre == genres[0]:
        genres_dictionary[genres[0]] += 1
    elif _genre == genres[1]:
        genres_dictionary[genres[1]] += 1
    elif _genre == genres[2]:
        genres_dictionary[genres[2]] += 1
    elif _genre == genres[3]:
        genres_dictionary[genres[3]] += 1 
    elif _genre == genres[4]:
        genres_dictionary[genres[4]] += 1


def create_directory_structure(path):
    for genre in genres:
        if os.path.isdir(path + "\\" + genre) is False:
            os.mkdir(path + "\\" + genre)
 

def organize_dataset(dataset_path):
    print(f"Organizing dataset found in {dataset_path}")

    path = os.path.join(parent_dir, PROCESSED_DATASET_PATH)
    
    if os.path.isdir(path) is False:
        print("Processed dataset folder does not exist. Creating directory...")             
        os.mkdir(path)
        create_directory_structure(path)
        print("Directory created")
    
    print("Adding files...")

    for file in glob.glob(dataset_path + "\drummer*\session*\*4-4.mid"):  
        """
            Here I am splitting the path to get the information from the file names
            eg:
                unprocessed_dataset\drummer1\session1\100_neworleans-secondline_94_beat_4-4.mid
                ['100', 'neworleans-secondline', '94', 'beat', '4-4.mid']
        """
        path_split = file.split("\\")
        file_information = path_split[3].split("_")
        bpm = file_information[2]

        # If bpm is not between 90 and 130, ignore the track
        # This is done to remove files with exaggerated bpm
        if(int(bpm) < 90 or int(bpm) > 130):
            continue

        genre = get_fuzed_genre(file_information[1])

        # Gathering data points that represent a beat belonging to the hiphop, funk, rock, and jazz styles
        # Each style is an aggrigation of all datapoints belonging to a variation of the above parent styles

        # If the MIDI file name contains beat
        if file_information[3] == 'beat':

            # If the file represents one of the genres specified above
            if get_fuzed_genre(file_information[1]) != "invalid":
                append_genre_count(file_information[1])    
                
                id = genres_dictionary[genre]
                bpm = file_information[2]
                track_name = str(id) + "_" + genre + "_" + bpm + ".mid"
                midi = m21.converter.parse(file)
                track = Midi_track(str(id), track_name, genre, bpm, midi)

                src = parent_dir + "\\" + file
                dest =  PROCESSED_DATASET_PATH + "\\" + genre + "\\" + track.name

                midi.write("midi", dest)
    
    print("Done!")


if __name__ == "__main__":
    print("__Cleaning dataset__")
    organize_dataset(UNPROCESSED_DATASET_PATH)

    for genre in genres:
        print(f"{genres_dictionary[genre]} files in {genre} genre")

    



