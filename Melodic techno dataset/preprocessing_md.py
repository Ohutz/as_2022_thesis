import os
import music21 as m21
import json
import tensorflow.keras as keras
import numpy as np

SAVE_DIR = "dataset_md"
DATASET_PATH = "my_dataset"
SINGLE_FILE_DATASET = "file_dataset_md"
MAPPING_PATH = "mappings_md.json"
SEQUENCE_LENGTH = 32
ACCEPTABLE_DURATIONS = [0.25, #16th
                        0.5,  #8th
                        0.75, #8th + 16th
                        1,    #1/4th
                        1.5,  #dotted 1/4th
                        2,    #1/2
                        3,    #3 1/4th
                        4     #whole note
                        ]

class MidiTrack:
    def __init__(self, track_id, name, genre, bpm, midi):
        self.track_id = track_id
        self.name = name
        self.genre = genre
        self.bpm = bpm
        self.midi = midi


def get_bpm(midi):
    parts = midi.getElementsByClass(m21.stream.Part)
    measures_part0 = parts[0][0]
    bpm = measures_part0[2].number
    return bpm


# Loads the songs in the specified directory
def load_songs(dataset_path):
    tracks = []
    count = 0

    # Loop and load all files in dataset using music21
    for path, subdir, files in os.walk(dataset_path):
        for file in files:
            if file[-3:] == "mid": # Check that the file is midi

                track_id = count
                name = file
                midi = m21.converter.parse(os.path.join(path, file))
                bpm = 122 #get_bpm(midi)

                track = MidiTrack(str(track_id), name, "", bpm, midi)

                tracks.append(track)
                count += 1

    return tracks


def has_acceptable_durations(track, acceptable_durations):
    counter = 0
    for note in track.midi.flat.notesAndRests:
        counter += 1
        if note.duration.quarterLength not in acceptable_durations:
            # If the duration is not within the accepted lengths, set it to 0.25 (16th)
            note.duration.quarterLength = 0.25

    return True


def encode_track(track, time_step=0.25):
    # p = 60, d = 1.0 -> [60, "_", "_", "_"]

    encoded_track = []


    for event in track.midi.flat.notesAndRests:

        if isinstance(event, m21.note.Note):        # Check if event is a note
            symbol = event.pitch.midi
        elif isinstance(event, m21.chord.Chord):    # Check if event is a chord
            symbol = "multi "

            for note in event.pitches:
                symbol += str(note.midi) + " "

            symbol += "end_multi"

        elif isinstance(event, m21.note.Rest):
            symbol = "r"

        # Convert note/rest into time series notation
        steps = int(event.duration.quarterLength / time_step)

        for step in range(steps):
            if step == 0:
                encoded_track.append(symbol)
            else:
                encoded_track.append("_")

    encoded_track = " ".join(map(str, encoded_track))

    return encoded_track


def preprocess(dataset_path):
    # todo Trim tacks to be of a specific length (Example 2 bars)

    # Load tracks
    print("Loading tracks..")
    tracks = load_songs(dataset_path)
    print(f"{len(tracks)} tracks loaded!")

    for track in tracks:
        # Filter out songs that have non-acceptable durations

        if not has_acceptable_durations(track, ACCEPTABLE_DURATIONS):
            continue

        # Encode track with music time series representation
        encoded_track = encode_track(track)

        # Save songs to text file
        save_path = os.path.join(SAVE_DIR, str(track.track_id))
        with open(save_path, "w") as fp:
            fp.write(encoded_track)


def load(file_path):
    with open(file_path, "r") as fp:
        track = fp.read()

    return track


def create_single_file_dataset(dataset_path, file_dataset_path, sequence_length):
    new_track_delimiter = "/ " * sequence_length
    tracks = ""

    # Load encoded tracks and add delimiters
    for path, _, files in os.walk(dataset_path):
        for file in files:
            file_path = os.path.join(path, file)
            track = load(file_path)
            tracks = tracks + track + " " + new_track_delimiter

    tracks = tracks[:-1]

    # Save string containing all dataset
    with open(file_dataset_path, "w") as fp:
        fp.write(tracks)

    return tracks


def create_mapping(tracks, mapping_path):
    mappings = {}
    # Identify the vocabulary
    tracks = tracks.split()
    vocabulary = list(set(tracks))

    # Create mappings
    for i, symbol in enumerate(vocabulary):
        mappings[symbol] = i

    # Save vocabulary to JSON
    with open(mapping_path, "w") as fp:
        json.dump(mappings, fp, indent=4)


def convert_tracks_to_int(tracks):
    int_tracks = []

    # Load mappings
    with open(MAPPING_PATH, "r") as fp:
        mappings = json.load(fp)

    # Cast tracks string into a list
    tracks = tracks.split()

    # Map tracks to integers
    for symbol in tracks:
        int_tracks.append(mappings[symbol])

    return int_tracks


def generate_training_sequences(sequence_length):
    # Load songs and map them to int
    tracks = load(SINGLE_FILE_DATASET)
    int_tracks = convert_tracks_to_int(tracks)

    # Generate the training sequences
    inputs = []
    targets = []

    num_sequences = len(int_tracks) - sequence_length

    for i in range(num_sequences):
        inputs.append(int_tracks[i: i + sequence_length])
        targets.append((int_tracks[i + sequence_length]))

    # One hot encode the sequences
    # inputs: (# of sequences, sequence_length)
    vocabulary_size = len(set(int_tracks))
    inputs = keras.utils.to_categorical(inputs, num_classes=vocabulary_size)
    targets = np.array(targets)

    return inputs, targets


def main():
    preprocess(DATASET_PATH)
    tracks = create_single_file_dataset(SAVE_DIR, SINGLE_FILE_DATASET, SEQUENCE_LENGTH)
    create_mapping(tracks, MAPPING_PATH)
    inputs, targets = generate_training_sequences(SEQUENCE_LENGTH)


if __name__ == "__main__":
    main()
    #track = m21.converter.parse("encoding_test.mid")
    #has_acceptable_durations(track, ACCEPTABLE_DURATIONS)



