import argparse
import tensorflow.keras as keras
import numpy as np
import json
from preprocessing_md import SEQUENCE_LENGTH, MAPPING_PATH, MidiTrack, encode_track
import music21 as m21

MODEL_PATH = "./Models/model1_md.h5"
TEMPERATURE = 0.7
GENERATE_NAME = "md_drums.mid"
NUM_STEPS = 100

class DrumsGenerator:

    def __init__(self, model_path=f"{MODEL_PATH}"):
        self.model_path = model_path
        self.model = keras.models.load_model(model_path)

        with open(MAPPING_PATH, "r") as fp:
            self._mappings = json.load(fp)

        self._start_symbols = ["/"] * SEQUENCE_LENGTH

    def generate_drums(self, seed, num_steps, max_sequence_length, temperature):

        # Create seed with start symbols
        seed = seed.split()
        rhythm = seed
        seed = self._start_symbols + seed

        # Map seed to integers
        seed = [self._mappings[symbol] for symbol in seed]

        for _ in range(num_steps):

            # Limit the seed to max_sequence_length
            seed = seed[-max_sequence_length:]

            # One-hot encode the seed
            one_hot_seed = keras.utils.to_categorical(seed, num_classes=len(self._mappings))
            # (1, max_sequence_length, num of symbols in the vocabulary)
            one_hot_seed = one_hot_seed[np.newaxis, ...]

            # Make a prediction
            probabilities = self.model.predict(one_hot_seed)[0]

        # Eg:  HH   SD   CYM  KD
            # [0.1, 0.2, 0.1, 0.6] -> 1

            output_int = self._sample_with_temperature(probabilities, temperature)

            # Update seed
            seed.append(output_int)

            # Map int to encoding {mappings.json}
            output_symbol = [k for k, v in self._mappings.items() if v == output_int][0]

            # Check whether we're at the end of a rhythm
            if output_symbol == "/":
                break

            # Update the rhythm
            rhythm.append(output_symbol)

        return rhythm


    def _sample_with_temperature(self, probabilities, temperature):

        # Temperature -> infinity Non-deterministic, increases creativity
        # Temperature -> 0 Deterministic, no creativity
        # Temperature -> 1 Neutral, as provided by the model

        predictions = np.log(probabilities) / temperature
        probabilities = np.exp(predictions) / np.sum(np.exp(predictions))

        choices = range(len(probabilities)) # [0, 1, 2, 3]
        index = np.random.choice(choices, p=probabilities)

        return index

    def save_drums(self, drums, step_duration=0.25, format="midi", file_name="drums.mid"):
        # Create a music21 stream
        stream = m21.stream.Stream()

        # Parse all the symbols in the sequence and create note/rest/chord objects
        # 60 _ _ _ r _ 62 _
        start_symbol = None
        step_counter = 1
        # skips will be used to pass all the objects that are between multi and end_multi symbols inclusive
        skips = 0
        processing_multi = False
        chord_event = m21.chord.Chord()

        for i, symbol in enumerate(drums):

            # Handle case in which we have a chord
            if symbol == "multi" or symbol == "end_multi" or processing_multi:

                if symbol == "multi":
                    processing_multi = True

                elif symbol == "end_multi":
                    processing_multi = False
                    d = m21.duration.Duration()
                    d.quarterLength = 0.25
                    chord_event.duration = d
                    stream.append(chord_event)
                    chord_event = m21.chord.Chord()
                    continue
                else:
                    note = m21.note.Note(int(symbol), quarterLenth=0.25)
                    chord_event.add(note)
                    continue

            # Handle case in which we have a note/rest
            if symbol != "_" or i + 1 == len(drums):

                # Ensure we're dealing with note/rest beyond the first one
                if start_symbol is not None and start_symbol != "multi":

                    quarter_length_duration = step_duration * step_counter

                    # Handle rest
                    if start_symbol == "r":
                        m21_event = m21.note.Rest(quarterLength=quarter_length_duration)

                    # Handle note
                    else:
                        m21_event = m21.note.Note(int(start_symbol), quarterLength=quarter_length_duration)

                    stream.append(m21_event)

                    # Reset the step counter
                    step_counter = 1

                start_symbol = symbol

            # Handle case in which we have a prolongation sign "_"
            else:
                step_counter += 1

        # Write the m21 stream to midi file
        stream.write(format, file_name)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("-mp", "--model-path", dest="model_path", default="./Models/model1_md.h5", help="Path to the model. Defaults to model1_md.h5")
    parser.add_argument("-g", "--genre", dest="genre", help="Genre of the sequence")
    parser.add_argument("-s", "--seed", dest="seed", help="Seed for the model to start")
    parser.add_argument("-t", "--temperature", dest="temperature", default=0.7, help="Temperature setting. Values closer to 0 result in less creative outputs, 1 is neutral. Default is 0.7")
    parser.add_argument("-ns", "--num-steps", dest="num_steps", default=100, help="Length of the generated sequence")
    parser.add_argument("-n", "--name", dest="sequence_name", default="md_drums.mid", help="Name of the generated sequence. Defaults to drums.mid")
    args = parser.parse_args()

    NUM_STEPS = int(args.num_steps)
    TEMPERATURE = float(args.temperature)

    dg = DrumsGenerator()
    midi = m21.converter.parse(args.seed)

    track = MidiTrack(1, "test_model", args.genre, 122, midi)

    seed = encode_track(track)
    print("Provided seed is: " + seed)

    drums = dg.generate_drums(seed, NUM_STEPS, SEQUENCE_LENGTH, TEMPERATURE)
    print(drums)
    dg.save_drums(drums)