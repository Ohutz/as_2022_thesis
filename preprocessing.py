import os
import music21 as m21
import json
import tensorflow.keras as keras
import numpy as np

DATASET_PATH = "groove_dataset_processed"#\\fuzed_rock"
SAVE_DIR = "groove_dataset_encoded"
SINGLE_FILE_DATASET_FOLDER = "Single Files"
SINGLE_FILE_DATASET = "file_dataset"
MAPPINGS_PATH = "mappings.json"
MAPPINGS_FOLDER = "Mappings"
SEQUENCE_LENGTH = 64
ACCEPTABLE_DURATIONS = [0.25, #16th
                        0.5,  #8th
                        0.75, #8th + 16th
                        1,    #1/4th
                        1.5,  #dotted 1/4th
                        2,    #1/2
                        3,    #3 1/4th
                        4     #whole note
                        ]

class MidiTrack:
    def __init__(self, track_id, name, genre, bpm, midi):
        self.track_id = track_id
        self.name = name
        self.genre = genre
        self.bpm = bpm
        self.midi = midi


"""
    Returns the bpm of a track
"""
def get_bpm(midi):
    parts = midi.getElementsByClass(m21.stream.Part)
    measures_part0 = parts[0][0]
    bpm = measures_part0[2].number
    return bpm

"""
    Loads tracks from the specified directory
"""
def load_songs(dataset_path):
    tracks = []
    count = 0

    # Loop and load all files in dataset using music21
    for path, subdir, files in os.walk(dataset_path):
        for file in files:
            if file[-3:] == "mid": # Check that the file is midi

                track_id = count
                name = file
                genre = name.split("_")[2]
                midi = m21.converter.parse(os.path.join(path, file))
                bpm = get_bpm(midi)

                track = MidiTrack(str(track_id), name, genre, bpm, midi)

                tracks.append(track)
                count += 1

    return tracks


"""
    Checks if all the notes in the given track have an acceptable duration
"""
def has_acceptable_durations(track, acceptable_durations):
    counter = 0
    for note in track.midi.flat.notesAndRests:
        counter += 1
        if note.duration.quarterLength not in acceptable_durations:
            # If the duration is not within the accepted lengths, set it to 0.25 (16th)
            note.duration.quarterLength = 0.25

    return True


"""
    Encodes the track with a resolution of time_step

    An encoded file looks like this:

        rock multi 63 57 25 end_multi _ _ _ 67 64 r multi 56 67 49 end_multi
"""
def encode_track(track, time_step=0.25):
    # p = 60, d = 1.0 -> [60, "_", "_", "_"]

    encoded_track = []

    for event in track.midi.flat.notesAndRests:

        if isinstance(event, m21.note.Note):        # Check if event is a note
            symbol = event.pitch.midi
        elif isinstance(event, m21.chord.Chord):    # Check if event is a chord
            symbol = "multi "

            for note in event.pitches:
                symbol += str(note.midi) + " "

            symbol += "end_multi"

        elif isinstance(event, m21.note.Rest):
            symbol = "r"

        # Convert note/rest into time series notation
        steps = int(event.duration.quarterLength / time_step)

        for step in range(steps):
            if step == 0:
                encoded_track.append(symbol)
            else:
                encoded_track.append("_")

    encoded_track = " ".join(map(str, encoded_track))

    return encoded_track


"""
    Preprocesses the dataset found in dataset_path
        1) Load tracks
        2) Check if each track has acceptable durations
        3) Encodes the tracks into tokenized strings
        4) Saves the encoded tracks into individual files

"""
def preprocess(dataset_path):
    # Load tracks
    print("Loading tracks..")
    tracks = load_songs(dataset_path)
    print(f"{len(tracks)} tracks loaded!")

    if os.path.isdir(SAVE_DIR) is False:
        print("Creating encoded dataset directory")             
        os.mkdir(SAVE_DIR)
        print("Directory created")


    for track in tracks:
        # Filter out songs that have non-acceptable durations

        if not has_acceptable_durations(track, ACCEPTABLE_DURATIONS):
            continue

        # Encode track with music time series representation
        encoded_track = encode_track(track)

        # Save songs to text file
        save_path = os.path.join(SAVE_DIR, str(track.track_id))
        with open(save_path, "w") as fp:
            fp.write(encoded_track)


"""
    Function to load a track from a specified path
"""
def load(file_path):
    with open(file_path, "r") as fp:
        track = fp.read()

    return track


"""
    Appends all the encoded tracks into one file and delimits them using the [/] symbol
    The delimiter is repeated for sequence_length times to signify the end of a sequence
"""
def create_single_file_dataset(dataset_path, file_dataset_path, sequence_length):
    new_track_delimiter = "/ " * sequence_length
    tracks = ""

    # Load encoded tracks and add delimiters
    for path, _, files in os.walk(dataset_path):
        for file in files:
            file_path = os.path.join(path, file)
            track = load(file_path)
            tracks = tracks + track + " " + new_track_delimiter

    tracks = tracks[:-1]

    single_file_dataset_path = os.path.join(SINGLE_FILE_DATASET_FOLDER, SINGLE_FILE_DATASET)

    if os.path.isdir(SINGLE_FILE_DATASET_FOLDER) is False:
        print("Creating Single Files directory")             
        os.mkdir(SINGLE_FILE_DATASET_FOLDER)
        print("Directory created")

    # Save string containing all dataset
    with open(single_file_dataset_path, "w") as fp:
        fp.write(tracks)

    return tracks

"""
    One-hot encodes all the tokens into distinct integers and stores the mappings in a json file
"""
def create_mapping(tracks, mapping_path):
    mappings = {}
    # Identify the vocabulary
    tracks = tracks.split()
    vocabulary = list(set(tracks))

    # Create mappings
    for i, symbol in enumerate(vocabulary):
        mappings[symbol] = i

    if os.path.isdir(MAPPINGS_FOLDER) is False:
        print("Creating Mappings directory")             
        os.mkdir(MAPPINGS_FOLDER)
        print("Directory created")


    path = os.path.join(MAPPINGS_FOLDER, MAPPINGS_PATH)

    # Save vocabulary to JSON
    with open(path, "w") as fp:
        json.dump(mappings, fp, indent=4)


"""
    Converts the tracks into integers so that the model can understand the data
"""
def convert_tracks_to_int(tracks):
    int_tracks = []

    mapping = os.path.join(MAPPINGS_FOLDER, MAPPINGS_PATH)

    # Load mappings
    with open(mapping, "r") as fp:
        mappings = json.load(fp)

    # Cast tracks string into a list
    tracks = tracks.split()

    # Map tracks to integers
    for symbol in tracks:
        int_tracks.append(mappings[symbol])

    return int_tracks


"""
    Generates training sequences from the converted single file dataset and creates inputs and targets
"""
def generate_training_sequences(sequence_length):
    
    single_file_path = os.path.join(SINGLE_FILE_DATASET_FOLDER, SINGLE_FILE_DATASET)

    # Load songs and map them to int
    tracks = load(single_file_path)
    int_tracks = convert_tracks_to_int(tracks)

    # Generate the training sequences
    inputs = []
    targets = []

    num_sequences = len(int_tracks) - sequence_length

    for i in range(num_sequences):
        inputs.append(int_tracks[i: i + sequence_length])
        targets.append((int_tracks[i + sequence_length]))

    # One hot encode the sequences
    # inputs: (# of sequences, sequence_length)
    vocabulary_size = len(set(int_tracks))
    inputs = keras.utils.to_categorical(inputs, num_classes=vocabulary_size)
    targets = np.array(targets)

    return inputs, targets


def main():
    preprocess(DATASET_PATH)
    tracks = create_single_file_dataset(SAVE_DIR, SINGLE_FILE_DATASET, SEQUENCE_LENGTH)
    create_mapping(tracks, MAPPINGS_FOLDER + "/" + MAPPINGS_PATH)


if __name__ == "__main__":
    main()
    #track = m21.converter.parse("encoding_test.mid")
    #has_acceptable_durations(track, ACCEPTABLE_DURATIONS)



