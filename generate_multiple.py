from preprocessing import load_songs, encode_track
from generate import DrumsGenerator

if __name__ == "__main__":

    # Load 50 tracks and take the first 30 items in sequence
    print("Loading tracks..")

    tracks = load_songs("groove_dataset_processed/fuzed_rock")

    tracks = tracks[:50]

    print(f"{len(tracks)} tracks loaded.")

    dg = DrumsGenerator()

    i = 1

    for track in tracks:
        seed = encode_track(track)
        seed = seed.split()
        seed = seed[:50]

        encoded_seed = " ".join(map(str, seed))

        # Use seeds to generate track
        drums = dg.generate_drums(encoded_seed, 250, 64, 0.7)
        dg.save_drums(drums, 0.25, file_name=f"Generated_sequences/groove_model1_rock/sequence{i}.mid")

        i += 1
        # Create single file dataset from tracks

        # Load already trained model 

        # Train model on new generated tracks

    