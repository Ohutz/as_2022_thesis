import argparse
import tensorflow.keras as keras
import tensorflow.keras.layers
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
import tensorflow as tf
import matplotlib.pyplot as plt
from preprocessing import generate_training_sequences, SEQUENCE_LENGTH
from tensorflow.python.framework.config import set_memory_growth
from sklearn.model_selection import train_test_split


OUTPUT_UNITS = 27
# Number of different values in mappings.json
LOSS = "sparse_categorical_crossentropy" #categorical_crossentropy
LEARNING_RATE = 0.001
NUM_UNITS = [128, 256] # can also add more layers [256, 256]
EPOCHS = 500
BATCH_SIZE = 64 # Amount of samples the network is going to see before running backpropogation
SAVE_MODEL_PATH = "Models/"
MODEL_NAME = "groove_model_rock_2.h5"
CHECKPOINTS_PATH = SAVE_MODEL_PATH + "Checkpoints/Groove"

def plot_history(history):
    fig, axs = plt.subplots(2)

    # Create accuracy subplot
    axs[0].plot(history.history["acc"], label="train accuracy")
    axs[0].plot(history.history["val_acc"], label="test accuracy")
    axs[0].set_ylabel("Accuracy")
    axs[0].legend(loc="lower right")
    axs[0].set_title("Accuracy eval")

    # Create accuracy subplot
    axs[1].plot(history.history["loss"], label="train error")
    axs[1].plot(history.history["val_loss"], label="test error")
    axs[1].set_ylabel("Error")
    axs[1].set_xlabel("Epoch")
    axs[1].legend(loc="upper right")        
    axs[1].set_title("Error eval")
    plt.tight_layout()

    plt.savefig(f"figures/{MODEL_NAME} figure.png")
    plt.show()

def build_model(output_units, num_units, loss, learning_rate):

    """
    # Create the model architecture
    input = keras.layers.Input(shape=(None, output_units))
    x = keras.layers.LSTM(num_units[0])(input)
    x = keras.layers.Dropout(0.2)(x)
    


    output = keras.layers.Dense(output_units, activation="softmax")(x)

    model = keras.Model(input, output)

    # Compile model
    model.compile(loss=loss,
                  optimizer=keras.optimizers.Adam(learning_rate=learning_rate),
                  metrics=["accuracy"])
    """
    model = keras.Sequential()
    model.add(keras.layers.LSTM(num_units[0], input_shape = (None, output_units), recurrent_dropout = 0.3, return_sequences = True))
    model.add(keras.layers.Dropout(0.3))
    model.add(keras.layers.Bidirectional(keras.layers.LSTM(num_units[1])))
    model.add(keras.layers.Dropout(0.3))
    model.add(keras.layers.Dense(output_units))
    model.add(keras.layers.Activation('softmax'))
    model.compile(loss=loss,
                  optimizer=keras.optimizers.Adam(learning_rate=learning_rate),
                  metrics=["accuracy"])
    
    keras.utils.plot_model(model, to_file = "model1.png", show_shapes=True, show_layer_names=True)


    model.summary()
    return model


def train(output_units=OUTPUT_UNITS, num_units=NUM_UNITS, loss=LOSS, learning_rate=LEARNING_RATE):

    filepath = CHECKPOINTS_PATH + "/{epoch:02d}-{loss:.4f}-" + MODEL_NAME

    checkpoint = ModelCheckpoint(
        filepath,
        monitor="val_loss",
        verbose=0,
        save_best_only=True,
        mode="min"
    )

    early_stopping = EarlyStopping(
        monitor="val_loss",
        patience=10,
        mode="min",
        min_delta=0.0001
    )

    callbacks_list = [checkpoint, early_stopping]

    # Generate training sequences
    inputs, targets = generate_training_sequences(SEQUENCE_LENGTH)

    x_train, x_test, y_train, y_test = train_test_split(inputs, targets, test_size=0.3)

    # Build the network
    model = build_model(output_units, num_units, loss, learning_rate)

    # Train the model
    history = model.fit(x_train, y_train, validation_data = (x_test, y_test), epochs=EPOCHS, batch_size=BATCH_SIZE, callbacks=callbacks_list)

    # Save the model
    model.save(SAVE_MODEL_PATH + MODEL_NAME)

    plot_history(history)


if __name__ == "__main__":

    tf.compat.v1.disable_v2_behavior()
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            for gpu in gpus:
                set_memory_growth(gpu, True)
        except RuntimeError as e:
            print(e)

    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output-units", dest="output_units", default=27, help="Output units, should be equal to number of mappings in mappings.json")
    parser.add_argument("-lr", "--learning-rate", dest="learning_rate", default=0.001, help="Learning rate")
    parser.add_argument("-e", "--num-epochs", dest="epochs", default=500, help="Number of epochs")
    parser.add_argument("-bs", "--batch-size", dest="batch_size", default=64, help="Amount of samples the network is going to see before running backpropogation")
    parser.add_argument("-n", "--model-name", dest="model_name", default="groove_model_rock_3.h5", help="Name of the model to be saved")

    args = parser.parse_args()
    OUTPUT_UNITS = int(args.output_units)
    LEARNING_RATE = float(args.learning_rate)
    EPOCHS = int(args.epochs)
    BATCH_SIZE = int(args.batch_size)
    MODEL_NAME = args.model_name

    train()
